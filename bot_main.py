#!/usr/bin/python3.6
import asyncio
import aiohttp
import tg_api
import inet
from bot_handler import BotHandler
import saves
import logging
import restart
from os import path
from random import shuffle
from datetime import datetime, timedelta


folder = path.dirname(path.realpath(__file__))

# TODO rewrite logging
logging.basicConfig(filename=f'{folder}bot.log',
                    format='%(asctime)s    %(levelname)s: %(message)s',
                    datefmt='%d/%m/%Y %H:%M:%S',
                    level=logging.INFO)
logging.info('Program started')
logging.debug(f'Current directory: {folder}')

ADMIN_ID = ['196846654']
ADMIN_COMMANDS = ['/admin', '/log', '/restart', '/clear_log', '/black_list']

RESTART_FLAG = 0
restart_str_list = ['Нет конечно!', 'Да, перезапуск!', 'Нет!', 'Неееет!']

BOT_TOKEN = '1111177297:AAFmHkTOff6Xg4D48NJJlKb69FLy3hYKFuo'
BOT_ID = BOT_TOKEN.split(':')[0]

bt_new_group = tg_api.InlineButtonBuilder('Добавить в группу', url="t.me/team_management_bot?startgroup=init")
kb_new_group = tg_api.InlineMarkupBuilder([[bt_new_group]])

kb_admin = tg_api.KeyboardBuilder([['/log', '/restart'], ['/clear_log', '/black_list']],
                                  one_time_keyboard=False)
kb_yesno = tg_api.KeyboardBuilder([['Да'], ['Нет']], one_time_keyboard=False)
bt_count = tg_api.InlineButtonBuilder('Сколько сдало?', callback_data='count')
bt_res = tg_api.InlineButtonBuilder('Посмотреть результаты', callback_data='res')

# TODO change to save dict
all_groups = saves.SaveSet(file_path=folder + 'groups.dat')
# TODO saving?
all_respondents = dict()
all_reports = dict()
_help_text1 = 'Какие? Чем мы можем помочь?\n\nОтветьте на это сообщение'
_help_text2 = 'Почему? Чем мы можем помочь?\n\nОтветьте на это сообщение'

ban = saves.SaveSet(file_path=folder + 'ban_list.dat')
LAST_USERS = {}
_if_less_ban = timedelta(seconds=0.5)

_bl_add = tg_api.InlineButtonBuilder('Добавить', callback_data='_bl+add')
_bl_add_text = 'В ответ на это сообщение напишите id пользователя, которого необходимо добавить:'
_bl_del = tg_api.InlineButtonBuilder('Удалить', callback_data='_bl+del')
_bl_del_text = 'В ответ на это сообщение напишите id пользователя, которого необходимо удалить:'
_bl_ids = tg_api.InlineButtonBuilder('Просмотреть все id', callback_data='_bl+ids')
bl_keyboard = tg_api.InlineMarkupBuilder([[_bl_add, _bl_del], [_bl_ids]])

internet_funcs = inet.Proxy(timeout=3,
                            filename=f'{folder}proxy.dat',
                            site_to_test=f'https://api.telegram.org/bot{BOT_TOKEN}/getMe')


async def find_proxy():
    results_temp = await asyncio.gather(internet_funcs.internet_check('http://example.org/'),
                                        internet_funcs.internet_check(f'https://api.telegram.org/bot{BOT_TOKEN}/getMe'))
    if results_temp[0]['site'] == 'http://example.org/':
        results = [results_temp[0]['result'], results_temp[1]['result']]
    else:
        results = [results_temp[1]['result'], results_temp[0]['result']]
    logging.debug(f'Internet test results: {results_temp[0]}, {results_temp[1]}')
    if not results[0]:  # internet connection test
        logging.error('No internet connection at all!')
        raise restart.InternetConnectionError
    elif results[1]:  # telegram without proxy connection test
        logging.info('Telegram works fine without proxy')
        return None
    else:
        result = await internet_funcs.loader()
        if result:
            logging.info('Loaded proxy from file')
            logging.debug(f'Loaded proxy: {result}')
            return result
        else:
            logging.info('Got no proxy from the file, will try to find a new one')
            results, _ = await asyncio.wait([internet_funcs.broker_find(), internet_funcs.pub_find()])
            logging.debug(f'Proxy find results: {results}')
        results = [i.result() for i in results if i.result() is not None]
        if results:
            logging.info('Find one!')
            logging.debug(f'Chose proxy: {results[-1]}')
            return results[-1]
        else:
            logging.error('Did NOT find any proxy')
            raise restart.InternetConnectionError


async def daily(bot):
    # TODO choose time
    global all_groups, all_reports, BOT_ID
    all_reports.clear()
    temp = set(all_groups.data)  # to avoid runtime error
    for i in temp:
        chat_member = await bot.get_chat_member(i, BOT_ID)
        if i in ban.data:
            all_groups.remove(i)
            asyncio.ensure_future(bot.leave_chat(i))
            continue
        elif chat_member['status'] == 'left' or chat_member['status'] == 'kicked':
            all_groups.remove(i)
            continue
        elif chat_member['status'] == 'restricted':
            if not chat_member['can_send_messages']:
                all_groups.remove(i)
                asyncio.ensure_future(bot.leave_chat(i))
                continue
        all_reports.update({i: []})
        bt_daily = tg_api.InlineButtonBuilder('Пройти опрос', url=f"t.me/team_management_bot?start=daily{i}")
        daily_keyboard = tg_api.InlineMarkupBuilder([[bt_daily], [bt_count, bt_res]])
        asyncio.ensure_future(asyncio.gather(bot.send_message(i, 'Самое время пройти ежедневный опрос!',
                                                              reply_markup=daily_keyboard), asyncio.sleep(1)))


async def logic(bot):
    global RESTART_FLAG, LAST_USERS

    # getting update

    update = await bot.get_updates()
    if update is None:
        LAST_USERS.clear()  # if no update comes - clear all the dict
        return None

    logging.debug(update)

    # parsing update
    text, command, admin_command, reply_to_message, callback_query = False, False, False, False, False
    one_msg_rp = False
    message_text, reply_text, data = '', '', ''
    reply_message_id, message_id = 0, 0
    callback_id = ''

    if 'callback_query' in update.keys():
        callback_query = True
        received_message = update['callback_query']['message']
        data = update['callback_query']['data']
        user_id = str(update['callback_query']['from']['id'])
        chat_id = str(received_message['chat']['id'])
        callback_id: str = update['callback_query']['id']
    else:
        received_message = update['message']
        user_id = str(received_message['from']['id'])
        chat_id = str(received_message['chat']['id'])
        message_type = list(received_message.keys())[4]
        if message_type == 'text':
            message_text = received_message['text']
            text = True
            if message_text[0] == '/':
                command = True
                if chat_id in ADMIN_ID and message_text in ADMIN_COMMANDS:
                    admin_command = True
        elif message_type == 'reply_to_message':
            reply_to_message = True
            message_text = received_message['text']
            reply_message_id = int(received_message['reply_to_message']['message_id'])
            reply_text = received_message['reply_to_message']['text']
            message_id = int(received_message['message_id'])
            if message_id-reply_message_id == 1:
                one_msg_rp = True
        elif message_type == 'left_chat_participant':  # if we were kicked out of the group
            if received_message['left_chat_participant']['username'] == 'team_management_bot':
                all_groups.remove(chat_id)
                logging.debug(f'I was kicked out of the group {chat_id}')

    user_name = received_message['from']['first_name']
    if "username" in received_message['from'].keys():
        user_nick = "@"+received_message['from']['username']
    else:
        user_nick = user_name

    # working with black_list

    if chat_id in ban.data:
        return None

    now = datetime.now()
    if chat_id in LAST_USERS.keys():  # if user already in dict
        LAST_USERS[chat_id][0] += 1  # +1 update
        LAST_USERS[chat_id][2] = now  # change last update time

        # check if its necessary to add user to black_list

        last_updates = LAST_USERS[chat_id]
        num_of_updates = last_updates[0]
        spam_bool = (last_updates[2] - last_updates[1]) / num_of_updates <= _if_less_ban  # how fast new messages coming
        if num_of_updates == 30 and spam_bool:
            logging.debug(f"Pretend that user {user_name} (id: {chat_id}) is spamming!")
            return asyncio.ensure_future(bot.send_message(chat_id, 'Пожалуйста, не спамьте!\n'
                                                                   'У меня вообще-то черный список есть...',
                                                          reply_markup=tg_api.ReplyKeyboardRemove))
        elif num_of_updates == 40 and spam_bool:
            return asyncio.ensure_future(bot.send_message(chat_id, 'Последний раз предупреждаю!',
                                                          reply_markup=tg_api.ReplyKeyboardRemove))
        elif num_of_updates == 49 and spam_bool:
            return asyncio.ensure_future(bot.send_message(chat_id, 'Еще одно сообщение и в бан!',
                                                          reply_markup=tg_api.ReplyKeyboardRemove))
        elif num_of_updates >= 50 and spam_bool:
            ban.add(chat_id)
            all_groups.remove(chat_id)
            logging.info(f'User {user_name} (id: {chat_id}) was added to blacklist')
            LAST_USERS.pop(chat_id)  # delete user from dict
            return asyncio.ensure_future(bot.send_message(chat_id, 'Вы были добавлены в черный список.'))
        elif num_of_updates > 30 and spam_bool:
            return None  # don't answer anything if we pretend that user is spamming

    elif chat_id not in ADMIN_ID:  # if user isn't admin...
        # add user to the dict with 3 parameters: number of updates, time of the first update, time of the last one
        LAST_USERS.update({chat_id: [1, now, now]})

    # working with commands

    if admin_command:
        if '/admin' == message_text:
            return asyncio.ensure_future(bot.send_message(user_id, f'Наконец то мой дорогой админ {user_name} '
                                                                   f'добрался до раздела админских возможностей!\n\n'
                                                                   f'• Напишите /log для получения файла логов,'
                                                                   f' /clear_log для того чтобы его отчистить\n'
                                                                   f'• Напишите /restart для '
                                                                   f'принудительной перезагрузки\n'
                                                                   f'• Напишите /black_list для '
                                                                   f'работы с черным списком\n',
                                                          reply_markup=kb_admin))
        elif '/log' == message_text:
            return asyncio.ensure_future(bot.send_file(user_id, f'{folder}bot.log', 'log.txt'))
        elif '/restart' == message_text:
            shuffle(restart_str_list)
            kb_restart = []
            for i in list(restart_str_list):
                kb_restart.append([i])
            RESTART_FLAG = 1
            return asyncio.ensure_future(bot.send_message(user_id, 'Вы уверены?',
                                                          reply_markup=tg_api.KeyboardBuilder(kb_restart)))
        elif '/clear_log' == message_text:
            with open(folder + 'bot.log', 'w'):  # log clearing
                pass
            logging.info('Cleared log')
            return asyncio.ensure_future(bot.send_message(user_id, 'Лог был отчищен!', reply_markup=kb_admin))
        elif '/black_list' == message_text:
            return asyncio.ensure_future(bot.send_message(user_id, 'Что сделать?', reply_markup=bl_keyboard))

    elif command:
        # TODO add comand for new group
        if '/start' == message_text:
            return asyncio.ensure_future(bot.send_message(chat_id, f"Приветствую, {user_name}! \n"
                                                                   f"Я бот, который поможет тебе "
                                                                   f"с managment'ом в твоей команде!",
                                                          reply_markup=kb_new_group))
        elif '/now' == message_text:
            all_reports.update({chat_id: []})
            bt_daily = tg_api.InlineButtonBuilder('Пройти опрос', url=f"t.me/team_management_bot?start=daily{chat_id}")
            daily_keyboard = tg_api.InlineMarkupBuilder([[bt_daily], [bt_count, bt_res]])
            return asyncio.ensure_future(bot.send_message(chat_id, 'ОпросЪ!',
                                                          reply_markup=daily_keyboard))
        elif '/start daily' == message_text[:12]:
            all_respondents.update({user_id: [message_text[12:], None, None, None, None]})
            return asyncio.ensure_future(bot.send_message(chat_id, f"Приветик, {user_name}!"
                                                                   f"\n\n"
                                                                   f"У тебя было в планах сегодня работать?",
                                                          reply_markup=kb_yesno))
        elif '/start@team_management_bot init' == message_text:
            if chat_id not in ban.data:
                all_groups.add(chat_id)
                logging.debug(f'I was add to group: {chat_id}')
                # TODO update message text
                return asyncio.ensure_future(bot.send_message(chat_id, 'Всем привет!\n'
                                                                       'Я бот, который поможет вам с менеджментом!\n'
                                                                       'Буду присылать вам опросы раз в день\n'
                                                                       'Если захотите провести внеплановый опрос, '
                                                                       'напишите /now\n'
                                                                       'Учтите, что при создание нового опроса, '
                                                                       'результаты предыдущего автоматически удаляются'
                                                                       '\nНу и плюс к тому же, я все еще в разработке'))
            else:
                return None
        elif '/help' == message_text:
            return asyncio.ensure_future(bot.send_message(chat_id, 'Все чрезвычайно просто:\n'
                                                                   '• добавь меня в группу, с помощью кнопки ниже\n'
                                                                   '• группе будет предлагаться опрос в 10:00 МСК\n'
                                                                   '• все опросы могут получить админы группы',
                                                          reply_markup=kb_new_group)
                                         )

    elif text:
        if chat_id in ADMIN_ID and message_text == 'Да, перезапуск!' and RESTART_FLAG:
            RESTART_FLAG = 0
            await bot.send_message(user_id, 'Перезапускаюсь...', reply_markup=kb_admin)
            raise restart.UserRestart

        elif chat_id in ADMIN_ID and RESTART_FLAG:
            RESTART_FLAG = 0
            return asyncio.ensure_future(bot.send_message(user_id, 'Перезапуск отменен', reply_markup=kb_admin))

        elif user_id in all_respondents.keys() and message_text == 'Да':
            for i in range(len(all_respondents[user_id])):
                if all_respondents[user_id][i] is None:
                    all_respondents[user_id][i] = True
                    break

            count_none = len([i for i in all_respondents[user_id] if i is not None])

            if count_none == 2:
                return asyncio.ensure_future(bot.send_message(chat_id, 'Вам понятно ваше задание?'))
            elif count_none == 3:
                return asyncio.ensure_future(bot.send_message(chat_id, 'Вы успеваете к дедлайну?'))
            elif count_none == 4:
                return asyncio.ensure_future(bot.send_message(chat_id, 'У вас возникли какие либо сложности?'))
            elif count_none == 5:
                return asyncio.ensure_future(bot.send_message(chat_id, _help_text1,
                                                              reply_markup=tg_api.Force_Reply))

        elif user_id in all_respondents.keys() and message_text == 'Нет':
            for i in range(len(all_respondents[user_id])):
                if all_respondents[user_id][i] is None:
                    all_respondents[user_id][i] = False
                    break

            count_none = len([i for i in all_respondents[user_id] if i is not None])

            if count_none == 2:
                temp_r, temp_g = str_report(user_name, user_nick, all_respondents.pop(user_id))
                all_reports[temp_g].append(temp_r)
                return asyncio.ensure_future(bot.send_message(chat_id, 'Хорошего отдыха!',
                                                              reply_markup=tg_api.ReplyKeyboardRemove))
            elif count_none == 3:
                temp_r, temp_g = str_report(user_name, user_nick, all_respondents.pop(user_id))
                all_reports[temp_g].append(temp_r)
                return asyncio.ensure_future(bot.send_message(chat_id, 'В скором времени с вами свяжутся.',
                                                              reply_markup=tg_api.ReplyKeyboardRemove))
            elif count_none == 4:
                return asyncio.ensure_future(bot.send_message(chat_id, _help_text2,
                                                              reply_markup=tg_api.Force_Reply))
            elif count_none == 5:
                temp_r, temp_g = str_report(user_name, user_nick, all_respondents.pop(user_id))
                all_reports[temp_g].append(temp_r)
                return asyncio.ensure_future(bot.send_message(chat_id, 'Так держать!',
                                                              reply_markup=tg_api.ReplyKeyboardRemove))
    elif reply_to_message:
        if reply_text == _bl_add_text and one_msg_rp and chat_id in ADMIN_ID:
            asyncio.ensure_future(bot.delete_message(chat_id, reply_message_id))
            if message_text.isdigit():
                if message_text not in ban.data:
                    ban.add(message_text)
                    return asyncio.ensure_future(bot.send_message(chat_id, f'Пользователь (id: {message_text}) '
                                                                           f'был добавлен в черный список',
                                                                  reply_markup=kb_admin))
                else:
                    return asyncio.ensure_future(bot.send_message(chat_id, f"Пользователь (id: {message_text}) уже "
                                                                           f"в черном списке",
                                                                  reply_markup=kb_admin))
            else:
                return asyncio.ensure_future(bot.send_message(chat_id, 'Id введен неверно! Он должен содержать'
                                                                       ' только цифры, и ничего более!',
                                                              reply_markup=kb_admin))
        elif reply_text == _bl_del_text and one_msg_rp and chat_id in ADMIN_ID:
            asyncio.ensure_future(bot.delete_message(chat_id, reply_message_id))
            if message_text.isdigit():
                if message_text in ban.data:
                    ban.remove(message_text)
                    return asyncio.ensure_future(bot.send_message(chat_id, f'Пользователь (id: {message_text}) '
                                                                           f'был удален из черного списка',
                                                                  reply_markup=kb_admin))
                else:
                    return asyncio.ensure_future(bot.send_message(chat_id, f"Пользователь (id: {message_text}) не "
                                                                           f"в черном списке",
                                                                  reply_markup=kb_admin))
            else:
                return asyncio.ensure_future(bot.send_message(chat_id, 'Id введен неверно! Он должен содержать'
                                                                       ' только цифры, и ничего более!',
                                                              reply_markup=kb_admin))
        elif (reply_text == _help_text1 or reply_text == _help_text2) and user_id in all_respondents.keys():
            temp_r, temp_g = str_report(user_name, user_nick, all_respondents.pop(user_id), message_text)
            all_reports[temp_g].append(temp_r)
            return asyncio.ensure_future(bot.send_message(chat_id, 'Ваш ответ записан. Вскоре с вами свяжутся.'))

    elif callback_query:
        if data[0] == '_':  # blacklist
            data = data[1:].split('+')
            asyncio.ensure_future(bot.callback_answer(callback_id))
            if data[1] == 'add':
                return asyncio.ensure_future(bot.send_message(chat_id, _bl_add_text,
                                                              reply_markup=tg_api.Force_Reply))
            elif data[1] == 'del':
                if ban.data:
                    return asyncio.ensure_future(bot.send_message(chat_id, _bl_del_text,
                                                                  reply_markup=tg_api.Force_Reply))
                else:
                    return asyncio.ensure_future(bot.send_message(chat_id, 'Черный список пуст!',
                                                                  reply_markup=kb_admin))
            elif data[1] == 'ids':
                if ban.data:
                    pretty_ids = ''
                    for i in ban.data:
                        pretty_ids += (i + ', ')
                    pretty_ids = pretty_ids[:-2]
                    return asyncio.ensure_future(bot.send_message(chat_id, f'Все id:  {pretty_ids}',
                                                                  reply_markup=kb_admin))
                else:
                    return asyncio.ensure_future(bot.send_message(chat_id, 'Черный список пуст!',
                                                                  reply_markup=kb_admin))
        elif data == 'count':
            asyncio.ensure_future(bot.callback_answer(callback_id, text=f'Сдало: {len(all_reports.get(chat_id, []))}'))
        elif data == 'res':
            member = await bot.get_chat_member(chat_id, user_id)
            if member['status'] == 'administrator' or member['status'] == 'creator':
                temp = all_reports.get(chat_id, 'Нет данных!')
                if temp:
                    temp = ''.join([i+'\n\n=====\n\n' for i in temp])
                    asyncio.ensure_future(bot.send_message(user_id, temp))
                    asyncio.ensure_future(bot.callback_answer(callback_id, text='Результаты в ЛС'))
                else:
                    asyncio.ensure_future(bot.callback_answer(callback_id, text='Нет данных!', show_alert=True))
            else:
                asyncio.ensure_future(bot.callback_answer(callback_id, text='Вы не администартор!', show_alert=True))


def str_report(respondent_name: str, respondent_nick: str, respondent_data: list, respondent_message=None):
    report = f'Респондент: {respondent_name}\n\n'
    report += f'Работает сегодня: {yes_or_no(respondent_data[1])}\n'
    if respondent_data[1] is False:  # doesnt work today
        report += f'-----\n{respondent_nick} сегодня отдыхает'
        return report, respondent_data[0]
    report += f'Задание понятно: {yes_or_no(respondent_data[2])}\n'
    if respondent_data[2] is False:  # task wasn't understand
        report += f'-----\nНеобходимо связаться с {respondent_nick}'
        return report, respondent_data[0]
    report += f'Успевает к дедлайну: {yes_or_no(respondent_data[3])}\n'
    if respondent_data[3] is False:  # deadline will not satisfy
        report += f'Не успевает к дедлайну потому что/как помочь: {respondent_message}\n'
        report += f'-----\nНеобходимо связаться с {respondent_nick}'
        return report, respondent_data[0]
    report += f'Есть проблемы: {yes_or_no(respondent_data[4])}\n'
    if respondent_data[4] is True:  # have problems
        report += f'Проблемы/как помочь: {respondent_message}\n'
        report += f'-----\nНеобходимо связаться с {respondent_nick}'
    else:
        report += f'-----\n{respondent_nick} отлично справляется!'
    return report, respondent_data[0]


def yes_or_no(boolean: bool):
    if boolean:
        return 'Да'
    else:
        return 'Нет'


async def task_manager():
    global proxy

    async with aiohttp.ClientSession() as session:
        tg_bot = BotHandler(BOT_TOKEN, session, proxy)
        task_bot = asyncio.ensure_future(logic(tg_bot), loop=loop)
        daily_message_task = asyncio.Future()
        time = loop.time()
        while True:
            if task_bot.done():  # check if our task done
                task_bot.result()  # will raise error if task finished incorrectly
                task_bot = asyncio.ensure_future(logic(tg_bot), loop=loop)
            if loop.time() - time >= 600.0:  # will start the task every 10 minutes
                time += 600.0
                daily_message_task = asyncio.ensure_future(daily(tg_bot), loop=loop)
            '''
            now = datetime.now()
            if 10 <= now.hour and 0 <= now.minute and daily_day <= now.day and daily_month == now.month:
                next_day = now + timedelta(days=1)
                daily_day, daily_month = next_day.day, next_day.month
                daily_message_task = asyncio.ensure_future(daily(tg_bot), loop=loop)
            '''
            if daily_message_task.done():
                daily_message_task.result()
            await asyncio.sleep(0)  # give control back to event loop


if __name__ == '__main__':
    loop = asyncio.get_event_loop()
    task_proxy = loop.create_task(find_proxy())
    try:
        proxy = loop.run_until_complete(task_proxy)
    except Exception as err:
        logging.critical(f'Restart caused: {type(err)}:{err}')
        loop.run_until_complete(asyncio.sleep(0.250))  # wait for all connections to close
        task_proxy.cancel()
        loop.stop()
        loop.close()
        asyncio.set_event_loop(asyncio.new_event_loop())
        restart.program(folder + 'bot_main.py', 10)
    else:
        if proxy:  # if we got proxy
            proxy = f'http://{proxy}'  # add http prefix to it
        else:
            proxy = None  # else - leave it None
        task_main = loop.create_task(task_manager())
        logging.info("Start main program")
        try:
            loop.run_until_complete(task_main)
        except Exception as err:
            logging.critical(f'Restart caused: {type(err)}:{err}')
        finally:
            loop.run_until_complete(asyncio.sleep(0.250))  # wait for all connections to close
            task_main.cancel()
            loop.stop()
            loop.close()
            asyncio.set_event_loop(asyncio.new_event_loop())
            restart.program(folder + 'bot_main.py', 10)
else:
    logging.critical(f'__Name__ is NOT equal main! It is {__name__}')
