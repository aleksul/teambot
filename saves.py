from os import path, stat


class SaveSet:
    def __init__(self, file_path: str):
        self.file_path = file_path
        if path.exists(file_path) and stat(file_path).st_size != 0:  # file exists and it is NOT empty
            with open(file_path, "r") as f:
                data = f.readlines()
            self.data = {i[:-1:] for i in data}
        else:
            self.data = set()

    def add(self, add_data: str):
        self.data.add(add_data)
        self.__rewrite()

    def remove(self, remove_data: str):
        self.data.discard(remove_data)
        self.__rewrite()

    def __rewrite(self):
        data_to_write = [i + '\n' for i in self.data]
        with open(self.file_path, 'w') as f:
            f.writelines(data_to_write)


class SaveDicts:
    def __init__(self, file_path: str, list_of_params: list):
        self.file_path = file_path
        self.fieldnames = list_of_params
        data = dict()
        for parameter in list_of_params:  # add keys to dict
            data.update({parameter: list()})

        if path.exists(file_path) and stat(file_path).st_size != 0:  # file exists and it is NOT empty
            with open(file_path, 'r') as f:
                read_data = f.readlines()
            for raw in read_data:  # for every read string
                raw = raw[:-1:]
                values = raw.split(',')  # get comma-separated-values
                for value, parameter in zip(values, list_of_params):
                    data[parameter].append(value)  # add value to the dict

        self.data = data

    def add(self, add_data: list):
        if len(add_data) != len(self.fieldnames):
            raise TypeError
        for value, parameter in zip(add_data, self.fieldnames):
            self.data[parameter].append(value)
        self.__append(add_data)

    def index(self, parameter: str, find_data: str):
        if parameter not in self.fieldnames:
            raise KeyError
        return self.data[parameter].index(find_data)

    def get(self, index: int, parameter=None):
        if parameter:
            if parameter not in self.fieldnames:
                raise KeyError
            return self.data[parameter][index]
        else:
            temp = dict()
            for parameter in self.fieldnames:
                temp.update({parameter: self.data[parameter][index]})
            return temp

    def remove(self, index: int):
        for parameter in self.fieldnames:
            self.data[parameter].pop(index)
        self.__rewrite()

    def __rewrite(self):
        list_to_write = [',']*len(self.data[self.fieldnames[0]])
        for i in range(len(list_to_write)):
            temp = []
            for parameter in self.fieldnames:
                temp.append(self.data[parameter][i])
            list_to_write[i].join(temp)
            list_to_write[i] += '\n'
        with open(self.file_path, 'w') as f:
            f.writelines(list_to_write)

    def __append(self, append_data: list):
        str_to_write = ','.join(append_data)+'\n'
        with open(self.file_path, 'a') as f:
            f.write(str_to_write)

